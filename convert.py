# 2.a 

# исходные данные
# data =   [(1, 2), (3, 4), (4, 89), (1, 1), (3, 5)] => 
# результат
# result =  {1: [2, 1], 3: [4, 5], 4: [89]}

# реализовать код преобразования
# def convert(data):
#     pass

# решить с помощью for in



data = [(1, 2), (3, 4), (4, 89), (1, 1), (3, 5)]

def convert(data):
	for i in data:
		print(f'I with index 0: {i[0]}', end=' ')
		print(f'I with index 1: {i[1]}')

    
convert(data)

# assert convert(data) = {1: [2, 1], 3: [4, 5], 4: [89]}

# default_dict