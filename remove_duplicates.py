#!/usr/bin/env python

# написать функцию для удаления дубликатов из итерируемого объекта,
# причем оставшиеся данные должны идти в том же порядке


from timeit import default_timer
# from collections import OrderedDict


# # 1

# start = default_timer()

# data = [1, 2, 3, 1, 4, 2, 1, 5, 8, 1, 2]

# def remove_duplicates(data):
#     '''Remove duplicates with converting to set
# 	and returns new list'''
#     result = list(set(data))
#     return result

# assert remove_duplicates(data) == [1, 2, 3, 4, 5, 8]

# print(f'1. Takes {default_timer() - start} ms')



# # 2

# start = default_timer()

# data = [1, 2, 3, 1, 4, 2, 1, 5, 8, 1, 2]

# def remove_duplicates(data):
#     '''Remove duplicates with converting to dict
#     using fromkeys method and returns converted new list'''
#     result = list(dict.fromkeys(data))
#     return result

# assert remove_duplicates(data) == [1, 2, 3, 4, 5, 8]

# print(f'2. Takes {default_timer() - start} ms')



# # 3

# start = default_timer()

# data = [1, 2, 3, 1, 4, 2, 1, 5, 8, 1, 2]


# def remove_duplicates(data):
#     '''
#     Remove duplicates using for loop and if statement with not in 
#     1. List comperhension
#     2. Siple using for loop and if statement, which one commented
#     '''
#     result = list()
#     [result.append(index) for index in data if index not in result]
#     return result
#     # result = list()
#     # for index in data:
#     #     if index not in result:
#     #         result.append(index)
#     # return result

# assert remove_duplicates(data) == [1, 2, 3, 4, 5, 8]

# print(f'3. Takes {default_timer() - start} ms')



# # 4

# start = default_timer()

# data = [1, 2, 3, 1, 4, 2, 1, 5, 8, 1, 2]

# def remove_duplicates(data):
#     '''
#     Remove duplicates using enumerate method 
#     and some futures from python language
#     '''
#     result = [v for i, v in enumerate(data)
#               if v not in data[:i]
#               ]
#     return result


# assert remove_duplicates(data) == [1, 2, 3, 4, 5, 8]

# print(f'4. Takes {default_timer() - start} ms')



# # 5

# start = default_timer()

# data = [1, 2, 3, 1, 4, 2, 1, 5, 8, 1, 2]

# def remove_duplicates(data):
#     '''
#     Remove duplcates using OrderedDict module (library)
#     '''
#     result = list(OrderedDict.fromkeys(data))
#     return result


# assert remove_duplicates(data) == [1, 2, 3, 4, 5, 8]

# print(f'5. Takes {default_timer() - start} ms')

